This project contains three core files:
  - run.py which produce our top Keggle result
  - Accuracy measuring.ipynb which is a jupyter notebook presenting accuracy measures we took to validate our prediction on different methods
  - Method Comparison.ipynb which presents our data engineering process, its exploration and results of different methods on those datasets (We did not include best accuracy dataset as its computation takes a lot of time)

WARNING:
All base methods return loss in terms of RMSE.
